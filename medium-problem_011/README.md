### PROBLEM 11 (medium)

This problem was asked by Twitter.

Implement an autocomplete system. That is, given a query string `s` and a set of all possible query strings, return all strings in the set that have s as a prefix.

For example, given the query string `de` and the set of strings [`dog`, `deer`, `deal`], return [`deer`, `deal`].

Hint: Try preprocessing the dictionary into a more efficient data structure to speed up queries.

---
### SOLUTION

There is a data structure called a "radix tree", which looks quite a bit like a combination of an adjacency matrix and a graph:

![graph](images/graph.png "graph")

`d` leads to `o` (d __o__ g) and `e` (d __e__ a l, d __e__ a r)
```
  e o
d 1 0
```
The `e` in deal and deer then lead to:
```
  e a
e 1 1
```
And so on.

Output:

![output](images/output.png "output")
