#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>

using namespace std;

struct numPairs {
	int low;
	int high;
};

struct numPairs simpleSort(const int num) {
	stringstream ss;
	ss << num;
	string str = ss.str();
	int idx = 0;
	char orig[5];
	orig[4] = '\0';
	for_each(str.begin(), str.end(), [&orig, &idx](char c) {
		orig[idx++] = c;
	});

	// good old bubble sort
	char temp;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3 - i; j++) {
			if(orig[j] > orig[j + 1]) {
				temp = orig[j];
				orig[j] = orig[j + 1];
				orig[j + 1] = temp;
			}
		}
	}

	struct numPairs foo;
	foo.low = stoi(orig);
	// reverse the digits
	temp = orig[0];
	orig[0] = orig[3];
	orig[3] = temp;

	temp = orig[1];
	orig[1] = orig[2];
	orig[2] = temp;

	foo.high = stoi(orig);
	return foo;
}

int steps(const int num) {
	if(num < 1000 || num > 9999) {
		return -1;	// invalid
	}
	int steps = 0, diff = 0, copy = num;
	do {
		numPairs nums = simpleSort(copy);
		diff = nums.high - nums.low;
		steps++;
		copy = diff;
	}
	while(diff != 6174);
	return steps;
}

int main() {
	cout << "4321: " << steps(4321) << endl;
	cout << "8426: " << steps(8426) << endl;
	cout << "3951: " << steps(3951) << endl;
	return 0;
}

