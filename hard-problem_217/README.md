### PROBLEM 217 (hard)

This problem was asked by Oracle.

We say a number is sparse if there are no adjacent ones in its binary representation. For example, `21` (10101) is sparse, but `22` (10110) is not. For a given input `N`, find the smallest sparse number greater than or equal to `N`.

Do this in faster than `O(N log N)` time.

---
### SOLUTION

We must analyze numbers in binary to see how to make them sparse. Let's take any random number ... 29. That becomes `00011101`. The next sparse number is `32`, which is `00100000`. Let's examine some numbers to see if a pattern or methodology emerges:

|Number|Binary|Sparse|Value|
|:-:|:-:|:-:|:-:|
|10|0000 1010|0000 1010|10|
|11|0000 1011|0001 0000|16|
|12|0000 1100|0001 0000|16|
|13|0000 1101|0001 0000|16|
|14|0000 1110|0001 0000|16|
|15|0000 1111|0001 0000|16|
|16|0001 0000|0001 0000|16|
|17|0001 0001|0001 0001|17|
|18|0001 0010|0001 0010|18|
|19|0001 0011|0001 0100|20|
|20|0001 0100|0001 0100|20|
|21|0001 0101|0001 0101|21|
|22|0001 0110|0010 0000|32|
|23|0001 0111|0010 0000|32|
|24|0001 1000|0010 0000|32|
|25|0001 1001|0010 0000|32|
|26|0001 1010|0010 0000|32|
|27|0001 1011|0010 0000|32|
|28|0001 1100|0010 0000|32|
|29|0001 1101|0010 0000|32|
|30|0001 1110|0010 0000|32|
|31|0001 1111|0010 0000|32|
|32|0010 0000|0010 0000|32|

The solution simply requires a little bit of tracking. The things that need to be tracked are:
- was a bit flipped to 0 because of adjacency?
- the value to AND the number

The algorithm is as follows:
- if a "1" is encountered at a certain position, determined if the next higher bit is also a "1"
- if the next higher bit is also a 1, flip my current bit to "0" and raise the "flipped" flag
- if my current bit is a "1" but the higher bit is a "0" and the "flipped" flag is set, flip my bit to "0" and set the higher bit to "1"; then flip all the lower bits to "0"

The solution has an efficiency of O(log N).

![output](images/output.png "output")
