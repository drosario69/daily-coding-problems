#include <iostream>

using namespace std;

int sparse(const int num) {
	int and_bits = 1, newnum = num;
	bool just_flipped = false;
	while(and_bits <= num) {
		int bit = newnum & and_bits;
		if(bit != 0) { // bit is set
			int high_bits = and_bits << 1;
			if((newnum & high_bits) != 0) { // higher bit also set
				just_flipped = true;
			} else {
				if(just_flipped) {
					newnum |= high_bits;
					just_flipped = false;
					int mask = high_bits - 1;
					newnum -= (newnum & mask); // shave off lower bits
				}
			}
		}		and_bits <<= 1;
	}
	return newnum;
}

int main(int argc, char **argv) {
	for(int i = 0; i<=32; i++) {
		cout << i << "? " << sparse(i) << endl;
	}
	cout << "38? " << sparse(38) << endl;
	cout << "152? " << sparse(152) << endl;
	return 0;
}
