#include <vector>
#include <deque>

class BinTree {
private:
	struct Node {
		Node *left = nullptr;
		Node *right = nullptr;
		int val;
	};
	Node *root = nullptr;
	const std::vector<int> *nums;

	void populate(int); // offset
	void showTree(const Node *, const int);
	bool cleanChildren(Node *); // flag lets you know if child should be deleted
public:
	BinTree(const std::vector<int>);
	void disp();
	void cleanTree();
};
