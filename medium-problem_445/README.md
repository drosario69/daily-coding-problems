## Problem 445 (medium)

This question was asked by BufferBox.

Given a binary tree where all nodes are either `0` or `1`, prune the tree so that subtrees containing all `0`s are removed.

For example, given the following tree:

```
   0
  / \
 1   0
    / \
   1   0
  / \
 0   0
```

should be pruned to:

```
   0
  / \
 1   0
    /
   1
```

We do not remove the tree at the root or its left child because it still has a `1` as a descendant.

---

## Solution

The solution seems obvious, but something must be kept in mind: a leaf node cannot delete itself. If we use depth-first recursion, we can reach the leaf node, and if it holds a `0` value, the node needs to be deleted. However, as stated, the node cannot delete itself. Rather, the parent node must be instructed that the node should be deleted. The parent node deletes the child leaf node and marks it as null. In C++, we use `nullptr` rather than `NULL`.

The question does not require that we populate the tree using any particular method. The method chosen was to represent the tree with a vector of integers. The tree from the above example is represented as follows: `{0, 1, 0, -1, -1, 1, 0, 0, 0}`. A value of `-1` indicates no node. This is interepreted as follows (step numbers indicate index/offset within the vector):

![tree](images/nodes.png "tree")

|vector|value|node #|comment|
|:-:|-:|:-:|-|
|0|0|1|root node|
|1|1|2|left of `1`|
|2|0|3|right of `1`|
|3|-1|n/a|left of `2`|
|4|-1|n/a|right of `2`|
|5|1|4|left of `3`|
|6|0|5|right of `3`|
|7|0|6|left of `4`|
|8|0|7|right of `4`|

Populating the tree isn't a simple matter of recursion. That explanation is beyond the scope of this question, but this method makes it easy to represent a tree with no code.

The result is displayed as a tree rotated 90 degrees counter-clockwise:

![output](images/output.png "output")
