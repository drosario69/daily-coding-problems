#include <iostream>
#include <vector>
#include <deque>
#include "BinTree.hpp"

using namespace std;

BinTree::BinTree(const vector<int> numv) {
	nums = &numv;
	root = new Node();
	populate(0);
}

void BinTree::populate(int offset) {
	deque<Node *> nodes;
	int pivot = 0;
	root->val = (*nums)[0]; // could do nums->at(0), but let's use operator overloading
	root->left = nullptr;
	root->right = nullptr;
	nodes.push_back(root);
	Node *ptr; // generic pointer

//	auto it = nums->begin();
	int index = 1; // where we pull from on nums vector
	while(index < nums->size()) {
		for(int i = 0; i <= pivot; i++) {
			// left side
			if(((*nums)[index]) != -1) {
				ptr = new Node();
				ptr->val = (*nums)[index];
				ptr->left = nullptr;
				ptr->right = nullptr;
				nodes[i]->left = ptr;
				nodes.push_back(ptr);
			}
			if(++index == (*nums).size()) {
				break;
			}

			// right side
			if((*nums)[index] != -1) {
				ptr = new Node();
				ptr->val = (*nums)[index];
				ptr->left = nullptr;
				ptr->right = nullptr;
				nodes[i]->right = ptr;
				nodes.push_back(ptr);
			}
			if(++index == (*nums).size()) {
				break;
			}
		}
		nodes.erase(nodes.begin(), nodes.begin() + pivot + 1);
		pivot = nodes.size() - 1;
	}
}

void BinTree::disp() {
	showTree(root, 0);
}

void BinTree::showTree(const Node *ptr, const int depth) {
	if(ptr->right != nullptr) {
		showTree(ptr->right, depth + 1);
	}
	cout << string(depth, '\t') << ptr->val << endl;
	if(ptr->left != nullptr) {
		showTree(ptr->left, depth + 1);
	}
}

void BinTree::cleanTree() {
	cleanChildren(root);
}

bool BinTree::cleanChildren(Node *ptr) {
	if(ptr->left == nullptr && ptr->right == nullptr) {
		if(ptr->val == 0) {
			return true;
		} else {
			return false;
		}
	}

	if(cleanChildren(ptr->left)) {
		delete ptr->left;
		ptr->left = nullptr;
	}
	if(cleanChildren(ptr->right)) {
		delete ptr->right;
		ptr->right = nullptr;
	}
	return false;
}
