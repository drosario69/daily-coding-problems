#include <iostream>
#include "vector"
#include "BinTree.hpp"

using namespace std;

int main() {
	vector<int> nums {0, 1, 0, -1, -1, 1, 0, 0, 0};
	BinTree tree(nums);
	cout << "Original tree:" << endl;
	tree.disp();
	tree.cleanTree();

	cout << endl << "Leaf 0's removed:" << endl;
	tree.disp();
	return 0;
}
