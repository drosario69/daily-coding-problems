#include <iostream>

using namespace std;

int match_idx(const int arr[], const int len) {
	// pass len because we can't determine the length of "int arr*"
	for(int idx = 0; idx <len; idx ++) {
		if(arr[idx] == idx) {
			return idx;
		}
	}
	return -1;
}

int main() {
	int arr[] = {-5, -3, 2, 3};
	cout << match_idx(arr, sizeof(arr)/sizeof(int)) << endl;
	return 0;
}
