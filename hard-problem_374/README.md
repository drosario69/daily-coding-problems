### PROBLEM 374 (hard)

This problem was asked by Amazon.

Given a sorted array `arr` of distinct integers, return the lowest index `i` for which `arr[i] == i`. Return `null` if there is no such index.

For example, given the array `[-5, -3, 2, 3]`, return `2` since `arr[2] == 2`. Even though `arr[3] == 3`, we return 2 since it's the lowest index.

---
### SOLUTION

I don't know if this was miscategorized, but it definitely is not hard.

We will not be able to satisfy the requirements of the problem. This is because C++ lacks an `Integer` object and only ccontains an `int` primitive. As such, we return a `-1` insteal of `null`.

![output](images/output.png "output")