#include <iostream>
#include "Node.hpp"

using namespace std;

Node::Node(const int num) {
	val = num;
	left = nullptr;
	right = nullptr;
}

Node::Node(int num, Node *l, Node *r) {
	val = num;
	left = l;
	right = r;
}

void Node::disp() {
	showTree(this, 0);
}

void Node::showTree(const Node *ptr, const int depth) {
	if(ptr->right != nullptr) {
		showTree(ptr->right, depth + 1);
	}
	cout << string(depth, '\t') << ptr->val << endl;
	if(ptr->left != nullptr) {
		showTree(ptr->left, depth + 1);
	}
}

int Node::sum(const int low, const int high) {
	lowSum = low;
	hiSum = high;
	total = 0;
	findSum(this);
	return total;
}

void Node::findSum(Node *ptr) {
	if(lowSum <= ptr->val && ptr->val <= hiSum) {
		total += ptr->val;
	}
	if(ptr->left != nullptr) {
		findSum(ptr->left);
	}
	if(ptr->right != nullptr) {
		findSum(ptr->right);
	}
}
