#include <iostream>
#include "Node.hpp"

using namespace std;

int main() {
	Node *root = new Node(
		5,
		new Node(
			3, new Node(2), new Node(4)
		),
		new Node(
			8, new Node(6), new Node(10)
		)
	);

	root->disp();
	cout << "23? " << root->sum(4, 9) << endl;
	return 0;
}
