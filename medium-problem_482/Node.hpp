class Node {
private:
	Node *left = nullptr;
	Node *right = nullptr;
	int val, total, lowSum, hiSum;
	void showTree(const Node *, const int);
	void findSum(Node *);
public:
	Node(const int);
	Node(int, Node *, Node *);
	void disp();
	int sum(const int, const int);
};
