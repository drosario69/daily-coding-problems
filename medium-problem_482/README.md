## Problem 482 (medium)

This problem was asked by Google.

Given a binary search tree and a range `[a, b]` (inclusive), return the sum of the elements of the binary search tree within the range.

For example, given the following tree:

```
    5
   / \
  3   8
 / \ / \
2  4 6  10
```

and the range `[4, 9]`, return `23` (5 + 4 + 6 + 8).

---

## Solution

Although the tree in the example is sorted, we do not know if all the trees will be sorted. If all the trees were sorted and, we could then optimize the search by not navigating to nodes whose values are less and more than the specified range. Since that is not specifically stated, we must assume that the tree is not sorted, requiring navigating to all nodes.

In our example, we print the tree on its side, rotated 90 degrees, to verify the tree is properly populated.

![output](images/output.png)
