#include <iostream>

using namespace std;

bool isPow4(const int num) {
	int copy = num;
	while(copy > 0) {
		if(copy & 1 == 1) {
			return true;
		}
		copy >>= 2;
	}
	return false;
}

int main() {
	cout << "32? " << isPow4(32) << endl;
	cout << "64? " << isPow4(64) << endl;
	cout << "8192? " << isPow4(8192) << endl;
	cout << "16384? " << isPow4(16384) << endl;
	return 0;
}

