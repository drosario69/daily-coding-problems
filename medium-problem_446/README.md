## Problem 446 (medium)

This problem was asked by Indeed.

Given a `32`-bit positive integer `N`, determine whether it is a power of four in faster than `O(log N)` time.

---
### Solution

Let's look at some numbers in binary which are powers of four:

|Exponent|Number|Binary|
|:-:|-:|-|
|4^0|1|0000 0000 0000 0001|
|4^1|4|0000 0000 0000 0100|
|4^2|16|0000 0000 0001 0000|
|4^3|64|0000 0000 0100 0000|
|4^4|256|0000 0001 0000 0000|
|4^5|1024|0000 0100 0000 0000|

We can see a pattern emerge. Every power of 4 has a single bit set to `1` and all others set to `0`. Only odd-numbered bits are set to one. The algorithm becomes a matter of performing a logical AND with 1, shifting bits twice to the right upon failure, and stopping if our shifted number becomes 0. Let's add a column to see if the efficiency is better than `O(long N)`:

|Exponent|Number|Binary|Big O|log N|
|:-:|-:|-|:-:|:-:|
|4^0|1|0000 0000 0000 0001|1|0|
|4^1|4|0000 0000 0000 0100|2|2|
|4^2|16|0000 0000 0001 0000|3|4|
|4^3|64|0000 0000 0100 0000|4|6|
|4^4|256|0000 0001 0000 0000|5|8|
|4^5|1024|0000 0100 0000 0000|6|10|
|4^6|4096|0001 0000 0000 0000|7|12|
|4^7|16384|0100 0000 0000 0000|8|14|

Sample output:

![output](images/output.png "output")
